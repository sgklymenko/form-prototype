# Form Abstract Prototype

Implementation of form validation in DDD style.

Набор классов для валидации форм.
Набор содержит абстрактные классы (AbstractValidator и AbstractForm).
Реализовано несколько разных валидаторов, и форма Регистрации.
Рендеринг формы отсутствует, только бизнес-логика

## Getting Started

Cloning project library

```bash
cd /path/to/projects
git clone https://sgklymenko@bitbucket.org/sgklymenko/form-prototype.git
cd form-prototype
```
