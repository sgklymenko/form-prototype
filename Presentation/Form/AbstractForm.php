<?php

namespace Presentation\Form;

use Presentation\Form\Validate\AbstractValidate,
	Presentation\Form\Validate\FormResult,
	Presentation\ValueObject\FormField,
	Domain\Validate\CodeValidateResult;

/**
 * @class abstract AbstractForm
 */
abstract class AbstractForm
{
	/** @var FormField[] $fields Associated array with objects of feild */
	private $fields = [];

	/** @var FormResult $result */
	private $result;

	/**
	 * constructor
	 * @todo field instanceof Presentation\ValueObject\FormField::constructor(AbstractValidate(Condition))
	 * @todo EmptyDataFormException
	 * @param array $data Values of fields
	 * @throws Exception('Empty data form')
	 */
	public function __construct(array $data = [])
	{
		if(empty($data)) {
			throw new \Exception('Empty data form');
		}

		$this->configure();
		$this->init($data);
	}

	/**
	 * @abstract function initfields() Init fields
	 * @example Presentation/Form/RegistrationForm.php
	 */
	abstract protected function configure();

	/**
	 * @param array $data Values of fields
	 */
	protected init(array $data)
	{
		// init
		foreach ($this->fields as $name => $field) {
			if (array_key_exists($name, $data)) {
				$field->setValue($data[$name]);
			}
		}
		$this->result = new FormResult();
	}

	/**
	 * @param array $data Values of fields
	 */
	private function addField(string $fieldName, FormField $field)
	{
		$this->fields[$fieldName] = $field;
		return $this;
	}
	/**
	 * Exec validate
	 */
	public function validate() : FormResult
	{
		/** @var FormField $field */
		foreach($this->$fields as $field) {
			/** @var FieldResult $fieldResult */
			$fieldResult = $field->validate();
			if (!$fieldResult->isValid()) {
				$this->result->setNotValid();
				$this->result->addCode($fieldResult->getCode());
			}
		}

		return $this->result;
	}

	/**
	 * Check form valid or not
	 */
	public function isValid() : boolean
	{
		return $this->result->isValid();
	}

	/**
	 * Add validator to field of form
	 * @param string $fieldName
	 * @param AbstractValidate $validator
	 * @return self
	 */
	 public function addValidator(string $fieldName, AbstractValidate $validator)
	 {
		 if (array_key_exists($fieldName, $this->$fields)) {
			 $field = $this->$fields[$fieldName];
			 $field->addValidator($validator);
		 } else {
			 throw new \Exception('Request not exists field of form'); // @TODO NotExistFieldFormException
		 }

		 return $this;
	 }

}
