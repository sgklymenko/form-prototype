<?php

namespace Presentation\Form;

use Presentation\Form\Validate\BaseResult,
	Domain\Validate\CodeValidateResult;

/**
 * @class FormValidateResult
 */
class FormResult extends BaseResult
{
	/**
	 * Get message fail value validate
	 * @upgrade Place for internalization
	 * @return string[]
	 */
	public function getMessages() : array
	{
		return $this->code;
	}

	/**
	 * @upgrade Replace to View & custom $type
	 * @return mixed
	 */
	static public function render(string $type = 'json')
	{
		return json_decode($this->getMessages());
	}
}
