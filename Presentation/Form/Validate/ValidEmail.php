<?php

namespace Presentation\Form\Validate;

use Domain\Validate\CodeValidateResult,
	Presentation\Form\Validate\AbstractValidate,
	Presentation\Form\Validate\BaseResult;

/**
 * @class ValidEmail
 */
class ValidEmail extends AbstractValidate
{
	/**
	 * Implementation exec validate field value
	 */
	public function validate($value, $values = [])
	{
		if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
			return new BaseResult(TRUE, CodeValidateResult::VALID_VALUE_DATA);
		}

		return new BaseResult(FALSE, CodeValidateResult::ERR_DUPLICATE_EMAIL);
	}
}
