<?php

namespace Presentation\Form\Validate;

use Presentation\Form\Validate\BaseResult;

/**
 * @class abstract AbstractValidate Validate & filtering value of feild
 */
abstract class AbstractValidate
{
	/**
	 * Implementation exec validate field value
	 */
	abstract public function validate($value, $values = []) : BaseResult;
}
