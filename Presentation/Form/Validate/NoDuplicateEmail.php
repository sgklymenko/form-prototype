<?php

namespace Presentation\Form\Validate;

use Domain\Validate\CodeValidateResult,
	Domain\Model\User,
	Presentation\Form\Validate\AbstractValidate,
	Presentation\Form\Validate\BaseResult;

/**
 * @class NoDuplicateEmail
 */
class NoDuplicateEmail extends AbstractValidate
{
	/**
	 * Implementation exec validate field value
	 */
	public function validate($value, $values = []) : BaseResult
	{
		/** @var User|NULL $user */
		$user = User::findByEmail($value);

		if ($user instanceof User) {
			return new BaseResult(FALSE, CodeValidateResult::ERR_DUPLICATE_EMAIL);
		}

		return new BaseResult(TRUE, CodeValidateResult::VALID_VALUE_DATA);
	}
}
