<?php

namespace Presentation\Form\Validate;

use Domain\Validate\CodeValidateResult;

/**
 * @class MinLegthValue
 */
class MinLegthValue extends AbstractValidate
{
	/** @var integer $min  */
	private $min;

	/**
	 * constructor
	 */
	public function __construct(integer $min)
	{
		$this->min = $min;
		return $this;
	}

	/**
	 * Implementation exec validate field value
	 */
	public function validate($value, $values = []) : BaseResult
	{
		if ($this->min < strlen($value)) {
			return new BaseResult(FALSE, CodeValidateResult::ERR_VALID_LENGTH);
		}

		return new BaseResult(TRUE, CodeValidateResult::VALID_VALUE_DATA);
	}
}
