<?php

namespace Presentation\Form\Validate;

use Domain\Validate\CodeValidateResult;

/**
 * @class BaseResult
 */
class BaseResult
{
	/** @var boolean $status */
	private $status;

	/** @var string[] $code */
	private $code;

	/**
	 * constructor
	 * @param boolean $status
	 * @param array $code
	 */
	public function __construct($status = TRUE, array $code = [CodeValidateResult::VALID_VALUE_DATA]) {
		$this->status = $status;
		$this->code = $code;
	}

	/**
	 * Get bool status result validate
	 * @return boolean
	 */
	public function isValid() {
		return $this->status;
	}

	/**
	 * Set status validate false
	 * @return self
	 */
	public function setNotValid() {
			$this->status = FALSE;

			return $this;
	}

	/**
	 * Get name variable for validate
	 * @return mixed
	 */
	public function getCode() : mixed
	{
			return $this->code;
	}

	/**
	 * Add code err validate
	 * @todo update
	 * @param mixed $code Code err validate
	 * @return self
	 */
	public function addCode(mixed $code)	{
		if ($this->code[0] !== CodeValidateResult::VALID_VALUE_DATA) {
			if (is_array($code)) {
				foreach ($code as $cod) {
					$this->code[] = $cod;
				}
			} else {
				$this->code[] = $code;
			}
		} else {
			if (is_array($code)) {
				$this->code = $code;
			} else {
				$this->code = [$code];
			}
		}

		return $this;
	}
}
