<?php

namespace Presentation\Form\Validate;

use Domain\Validate\CodeValidateResult,
	Presentation\Form\Validate\AbstractValidate,
	Presentation\Form\Validate\BaseResult,
	Presentation\ValueObject\FormField;

/**
 * @class ConfirmValue
 */
class ConfirmValue extends AbstractValidate
{
	/** @var string $match  */
	private $match;

	/**
	 * constructor
	 */
	public function __construct(FormField $feild)
	{
		$this->match = $feild->getValue();
		return $this;
	}
	/**
	 * Implementation exec validate field value
	 */
	public function validate($value, $values = [])
	{
		if (0 !== strcmp($value, $this->match)) {
			return new BaseResult(FALSE, CodeValidateResult::ERR_NOT_CONFIRM);
		}

		return new BaseResult(TRUE, CodeValidateResult::VALID_VALUE_DATA);
	}
}
