<?php

namespace Presentation\Form;

use Presentation\ValueObject\FormField,
	Presentation\Form\Validate\ValidEmail,
	Presentation\Form\Validate\NotDuplicateEmail,
	Presentation\Form\Validate\MinLegthValue,
	Presentation\Form\Validate\ConfirmValue;

/**
 * @class Registration
 */
class Registration extends AbstractForm
{
	/**
	 * init RegistrationForm fields
	 */
	protected function configure() {
		// Email field
		$this->addField('email', new FormField(
	 	 		'email',
	 			[
					new ValidEmail(),
					new NotDuplicateEmail()
				]
 		));
		// Password field
		$this->addField('password', new FormField(
 	 		'password',
 			[ new MinLegthValue(6) ]
 		));
		// Confirm password field
		$this->addField('pass_confirm', new FormField(
 	 		'pass_confirm',
 			[ new ConfirmValue($this->fields['password']) ]
 		));
	}

}
