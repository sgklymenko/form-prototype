<?php

namespace Presentation\ValueObject;

use Presentation\Form\Validate\AbstractValidate,
	Presentation\ValueObject\FieldResult;

/**
 * @class FormField
 */
class FormField extends AbstractValidate
{
	/** @var string $name Field name */
	private $name;

	/** @var AbstractValidate[] $validators */
	private $validators;

	/** @var mixed $value Field value */
	private $value;

	/** @var FormFieldType $type Field type for custom rendering */
	private $type;

	/** @var FieldResult $result */
	private $result;

	/**
	 * constructor
	 * @todo add check exist $name
	 * @todo novalidate
	 */
	public function __construct(string $name, $validators = [], $value = NULL, FormFieldType $type = FormFieldType::FIELD_TYPE_INPUT)
	{
		$this->name = $name;
		$this->validators = $validators;
		$this->value = $value;
		$this->type = $type;
		$this->result = new FieldResult();
	}

	public function setValue($value)
	{
		$this->value = $value;
	}

	public function addValidator(AbstractValidate $validator)
	{
		$this->validators[] = $validator;
	}

	/**
	 * Feild validate
	 */
	public function validate() : FieldResult
	{
		/** @var AbstractValidate $validator */
		foreach($this->validators as $validator) {
			/** @var BaseResult $result */
			$result = $validator->validate();
			if (!$result->isValid()) {
				$this->result->setNotValid();
				$this->result->addCode($result->getCode());
			}
		}

		return $this->result;
	}

	/**
	 * Feild name
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @TODO maybe delete
	 */
	public function getResult() : FieldResult
	{
		return $this->result;
	}

}
