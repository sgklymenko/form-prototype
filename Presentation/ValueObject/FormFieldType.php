<?php

namespace Presentation\ValueObject;

/**
 * For render
 * @todo maybe move anoter place
 * @class FormFieldType
 */
class FormFieldType
{
	/** @const string */
	const FIELD_TYPE_INPUT		= 'input';

	/** @const string */
	const FIELD_TYPE_TEXTAREA	= 'textarea';

	/** @const string */
	const FIELD_TYPE_PASSWORD	= 'password';

	/** @const string */
	const FIELD_TYPE_DATETIME	= 'datepicker';
}
