<?php

namespace Domain\Validate;

/**
 * List validation code error
 * @todo move replace
 * @class CodeValidateResult
 */
class CodeValidateResult
{
	/** @const string Data is valid */
	const VALID_VALUE_DATA			= 'VALID_VALUE_DATA';

	/** @const string Empty value */
	const ERR_VALID_EMPTY				= 'ERR_VALID_EMPTY';

	/** @const string Feild length value */
	const ERR_VALID_LENGTH			= 'ERR_VALID_LENGTH';

	/** @const string Not mail*/
	const ERR_VALID_EMAIL				= 'ERR_VALID_EMAIL';

	/** @const string Duplicate mail*/
	const ERR_DUPLICATE_EMAIL		= 'ERR_DUPLICATE_EMAIL';

	/** @const string Values ​​don't match */
	const ERR_NOT_CONFIRM				= 'ERR_NOT_CONFIRM';

	/* @const string Values ​​don't match
	const ERR_VALID_TODO				= 'ERR_VALID_TODO';*/

	/** @const string Forbidden symbol */
	const ERR_FORBIDDEN_SYMBOL			= 'ERR_FORBIDDEN_SYMBOL';

	/** @const string Forbidden IP value */
	const ERR_FORBIDDEN_ADDR			= 'ERR_FORBIDDEN_ADDR';

	/** @const string Empty value */
	const ERR_VALID_OTHER				= 'ERR_VALID_OTHER';

}
